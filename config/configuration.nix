# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ lib, config, pkgs, user, hostname, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "${hostname}";
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Enable flakes
  nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  # Allow some unfree packages
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "discord"
    "megasync"
    "nvidia-persistenced"
    "nvidia-settings"
    "nvidia-x11"
    "spotify-unwrapped"
    "zoom"
  ];

  # Set your time zone.
  time.timeZone = "Europe/Lisbon";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "pt-latin9";
  };

  hardware.opengl.driSupport32Bit = true;

  # Enable sound.
  sound.enable = true;

  # User config
  users.users = {
    root = {
      shell = pkgs.zsh;
      password = "123";
    };

    "${user}" = {
      isNormalUser = true;
      extraGroups = [
        "wheel"
        "docker"
        "libvirtd"
      ];
      shell = pkgs.zsh;
      password = "123";
    };
  };

  # Fonts
  fonts = {
    enableDefaultFonts = false;
    fonts = with pkgs; [
      noto-fonts
      font-awesome
      hack-font
      material-icons
    ];

    fontconfig.defaultFonts = {
      serif = [ "Noto Serif" ];
      sansSerif = [ "Noto Sans Serif" ];
      monospace = [ "Hack Nerd Font" "Noto Sans Mono" ];
      emoji = [ "Noto Color Emoji" ];
    };
  };
 
  environment.systemPackages = with pkgs; [
    # Core
    bash
    exa
    git
    htop
    ripgrep
    vim
    zellij

    # Virtualization
    docker
    libvirt

    # Sound
    pipewire
  ];

  programs = {
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };

    zsh.enable = true;
  };

  # Enable the OpenSSH daemon.
  services = {
    gnome.gnome-keyring.enable = true;

    openssh = {
      enable = true; # TODO Change this
      permitRootLogin = "yes";
    };

    pipewire = {
      enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
    };

    printing.enable = true;

    xserver = {
      displayManager.sddm = {
        autoNumlock = true;
        enable = true;
        enableHidpi = true;
      };
      enable = true;
      layout = "pt";
      libinput.enable = true;
      libinput.touchpad.naturalScrolling = true;
      videoDrivers = [
        #"i915"
        #"nvidia"
      ];
      windowManager = {
        xmonad = {
          enable = true;
          enableContribAndExtras = true;
        };
      };
    };
  };

  virtualisation = {
    docker = {
      enable = true;
      enableNvidia = true;
    };

    libvirtd.enable = true;
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?
}

