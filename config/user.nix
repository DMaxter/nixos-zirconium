{ pkgs, user, dotfiles, lib, ... }:

{
  home.packages = with pkgs; [
    # Browsers
    firefox
    chromium
    
    # Messaging apps
    ferdi
    discord

    # Misc
    alacritty
    arandr
    browserpass
    docker-compose
    dunst
    i3lock
    imagemagick
    kodi
    libreoffice-fresh
    megasync
    mu
    nitrogen
    pavucontrol
    picom
    playerctl
    qemu
    ranger
    redshift
    rofi
    spotify-unwrapped
    sshfs
    trayer
    wine
    xorg.xbacklight
    xmobar
    zoom-us
  ];

  # Programs
  programs = {
    emacs.enable = true;
    password-store.enable = true;
  };

  # Services
  services = { 
    flameshot.enable = true;
 
    kdeconnect = {
      enable = true;
      indicator = true;
    };

    syncthing = {
      enable = true;
    };
  };

  xdg = {
    enable = true;
    userDirs = {
      createDirectories = true;
      enable = true;
    };
  };

  # Mount dotfiles
  home.activation.dotfiles = lib.hm.dag.entryAfter["writeBoundary"] ''
    cd
    if [ ! -d "dotfiles" ]; then
      git clone --bare ${dotfiles} dotfiles
      git --git-dir=dotfiles --work-tree=. checkout master .
      git --git-dir=dotfiles --work-tree=. submodule update --init --recursive
    fi
  '';
}

