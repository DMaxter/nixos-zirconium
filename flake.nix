# flake.nix
#
# Author: Daniel Matos <daniel_matos@outlook.pt>
# URL:    https://gitlab.com/DMaxter/nixos-zirconium
#
# The file that helps powering my laptop

{
  description = "NixOS deployment for Zirconium";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    home.url = "github:nix-community/home-manager";
  };

  outputs = inputs @ { self, ... }: 
  let
    inherit (inputs.nixpkgs) lib;
    hostname = "zirconium";
    user     = "dmaxter";
    system   = "x86_64-linux";
    dotfiles = "https://gitlab.com/DMaxter/dotfiles";
  in {
    nixosConfigurations."${hostname}" = inputs.nixpkgs.lib.nixosSystem {
      inherit system;
      extraArgs = { inherit hostname user; };
      modules = [
        ./config/configuration.nix
        inputs.home.nixosModules.home-manager {
          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            extraSpecialArgs = { inherit user dotfiles; };
            users.${user} = import ./config/user.nix;
          };
        }
      ];
    };
  };
}
